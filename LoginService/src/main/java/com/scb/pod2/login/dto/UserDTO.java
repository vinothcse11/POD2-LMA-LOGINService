package com.scb.pod2.login.dto;

public class UserDTO {
	private String emailId;
	private String password;
	
	public UserDTO() {}
	public UserDTO(String emailId, String password) {
		super();
		this.emailId = emailId;
		this.password = password;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
